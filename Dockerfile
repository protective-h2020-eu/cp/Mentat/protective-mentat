# enriched extractor module
FROM registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/mentat-enriched-extractor:1.1.0 as mee

# trustapi plugin image
FROM registry.gitlab.com/protective-h2020-eu/enrichment/ti-trust/protective-trustapi:1.1.0 as pta

# base mentat
FROM registry.gitlab.com/protective-h2020-eu/cp/mentat/mentat:1.0-beta.1

# copying plugin contents
COPY --from=mee / /
COPY --from=pta / /

#install zeromq, necessary to fork events to the correlation pipeline
RUN pip install pyzmq
# install python module for trustapi
RUN pip install /tmp/trustapi

CMD ["/etc/mentat/scripts/start.sh"]
